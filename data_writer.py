import csv

from utils_for_genetic import find_the_best_and_the_worst_in_population


def create_file(filename):
    file = open(filename, 'w', newline='')
    writer = csv.writer(file, delimiter=';')
    writer.writerow(["number", "the best", "the worst", "average"])
    return writer, file


def write_population(population, number, writer):
    avg = sum(p.distance for p in population)/len(population)
    b, w = find_the_best_and_the_worst_in_population(population)
    writer.writerow([number, b.distance, w.distance, avg])
