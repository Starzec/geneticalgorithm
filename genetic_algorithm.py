import random

from data_writer import create_file, write_population
from greedy_algorithm import greedy_algorithm_n_best
from models import Individual
from utils import generate_genotype, count_distance
from utils_for_genetic import select_tournament, ordered_crossover, mutate_swap, select_roulette, \
    find_the_best_and_the_worst_in_population, append_random_individuals


def genetic_algorithm(pop_size, cities, tour, iterations, px, pm, filename, percent_of_new_ind):
    writer, file = create_file(filename)
    length = len(cities)
    population = [generate_genotype(length) for _ in range(int(0.8*pop_size))]
    g = greedy_algorithm_n_best(cities, pop_size - len(population))
    population = population + g
    while len(population) != pop_size:
        population.append(g[0])
    population = [Individual(p, count_distance(cities, p)) for p in population]
    i = 0
    the_best, the_worst = find_the_best_and_the_worst_in_population(population)
    while i < iterations:
        print("Iteration : " + str(i))
        write_population(population, i + 1, writer)
        new_population = [the_best]
        if i % 10 == 0:
            append_random_individuals(new_population, pop_size, percent_of_new_ind, cities)
        while len(new_population) != pop_size:
            genotype1 = select_tournament(population, tour, pop_size)
            # genotype1 = select_roulette(population)
            genotype2 = select_tournament(population, tour, pop_size)
            # genotype2 = select_roulette(population)
            rand = random.random()
            if rand <= px:
                genotype1 = ordered_crossover(genotype1, genotype2)
            mutate_swap(genotype1, pm)
            new_population.append(Individual(genotype1, count_distance(cities, genotype1)))
        b, w = find_the_best_and_the_worst_in_population(new_population)
        if the_best.distance >= b.distance:
            the_best = b
        if the_worst.distance <= w.distance:
            the_worst = w
        population = new_population
        i += 1
    file.close()
    return the_best, the_worst
