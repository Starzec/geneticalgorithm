from utils import count_distance_between_cities, count_distance


def greedy_algorithm(start_city, cities):
    length = len(cities)
    left_cities = {}
    genotype = [start_city]
    for i in range(1, length + 1):
        left_cities[i] = i
    del left_cities[start_city]
    for i in range(1, length):
        lowest_distance = float('inf')
        lowest_distance_city = -1
        for k in left_cities:
            actual_distance = count_distance_between_cities(cities[start_city], cities[k])
            if lowest_distance >= actual_distance:
                lowest_distance = actual_distance
                lowest_distance_city = k
        genotype.append(lowest_distance_city)
        del left_cities[lowest_distance_city]
        start_city = lowest_distance_city
    return genotype


def greedy_algorithm_for_every_city(cities):
    b = greedy_algorithm(1, cities)
    w = greedy_algorithm(1, cities)
    ci = 1
    all_solutions = []
    for c in cities:
        g = greedy_algorithm(c, cities)
        dist = count_distance(cities, g)
        if dist < count_distance(cities, b):
            b = g
            ci = c
        if dist > count_distance(cities, w):
            w = g
        all_solutions.append(dist)
    return b, ci, w, all_solutions


def greedy_algorithm_n_best(cities, n):
    greedy_solutions = [greedy_algorithm(c + 1, cities) for c in range(len(cities))]
    greedy_solutions.sort(key=lambda s: count_distance(cities, s))
    return greedy_solutions[0:n]
