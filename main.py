import math

from genetic_algorithm import genetic_algorithm
from reader import read_data
import time
import csv


def find_the_best_and_the_worst(tuples):
    best_from_tuples = tuples[0][0]
    worst_from_tuples = tuples[0][1]
    sum_of_distance = 0
    for tup in tuples:
        if best_from_tuples.distance > tup[0].distance:
            best_from_tuples = tup[0]
        if worst_from_tuples.distance < tup[1].distance:
            worst_from_tuples = tup[1]
        sum_of_distance += tup[0].distance
    return best_from_tuples, worst_from_tuples, sum_of_distance/len(tuples)


def count_standard_deviation(tuples, average):
    return math.sqrt(sum((average - tup[0].distance) ** 2 for tup in tuples)/len(tuples))


if __name__ == "__main__":
    start_time = time.time()
    cities = read_data("kroA100.tsp")
    list_of_tuples = []
    i = 0
    while i < 10:
        the_best, the_worst = genetic_algorithm(pop_size=100, cities=cities, tour=10, iterations=1000, px=0.7, pm=0.001,
                                                filename='statistic_result_kroA100_number: ' + str(i + 1) + '.csv', percent_of_new_ind=0.2)
        list_of_tuples.append((the_best, the_worst))
        i += 1
    best, worst, avg = find_the_best_and_the_worst(list_of_tuples)
    std = count_standard_deviation(list_of_tuples, avg)
    with open('kroA100_results_turniej.csv', 'w', newline='') as file:
        writer = csv.writer(file, delimiter=';')
        writer.writerow(["the best", "the worst", "average", "deviation", "pop size", "px", "pm", "iterations", "tour"])
        writer.writerow([best.distance, worst.distance, avg, std, 100, 0.7, 0.001, 1000, 40])
    print("--- %s seconds ---" % (time.time() - start_time))
    print(best)
    print(worst)
    print(avg)
    print(std)
    # the_best, the_worst = genetic_algorithm(pop_size=100, cities=cities, tour=10, iterations=10, px=0.95, pm=0.01,
    #                                       filename='statistics.csv', percent_of_new_ind=0.0) # dobry wynik w miarę
    # print(the_best)
    # print(the_best.genotype)
    # print(the_worst)
    print("--- %s seconds ---" % (time.time() - start_time))


