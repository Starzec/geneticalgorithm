class Individual:

    def __init__(self, genotype, distance):
        self.genotype = genotype
        self.distance = distance

    def __str__(self):
        return "Individual distance is: " + str(self.distance)


class City:

    def __init__(self, number, x, y):
        self.number = number
        self.x = x
        self.y = y

    def __str__(self):
        return "City number: " + str(self.number) + " Coordinates x: " + str(self.x) + " y: " + str(self.y)
