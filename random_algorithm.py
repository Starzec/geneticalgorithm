from utils import generate_genotype, count_distance


def random_algorithm(iterations, cities):
    gen = generate_genotype(len(cities))
    d = count_distance(cities, gen)
    the_best = d
    the_best_gen = gen
    the_worst = d
    all_solutions = []
    for i in range(iterations):
        g = generate_genotype(len(cities))
        d = count_distance(cities, g)
        if d < the_best:
            the_best = d
            the_best_gen = g
        if the_worst < d:
            the_worst = d
        all_solutions.append(d)
    return the_best, the_best_gen, the_worst, all_solutions
