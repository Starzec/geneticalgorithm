from models import City


def read_data(filename):
    f = open("TSP/" + filename)
    lines = [s for s in f.readlines() if s != "\n"]
    start_index = 0
    for i, line in enumerate(lines):
        if "NODE_COORD_SECTION" in line:
            start_index = i + 1
    cities = {}
    for line in lines[start_index:-1]:
        line = line.lstrip()
        line = ' '.join(line.split())
        number, x, y = line.split(" ")
        number = int(number)
        x = float(x)
        y = float(y)
        cities[number] = City(number, x, y)
    f.close()
    return cities
