import math
import time

from greedy_algorithm import greedy_algorithm, greedy_algorithm_for_every_city
from random_algorithm import random_algorithm
from reader import read_data
from utils import count_distance

if __name__ == "__main__":
    start_time = time.time()
    cities = read_data("fl417.tsp")
    the_best, the_best_gen, the_worst, all_solutions = random_algorithm(10000, cities)
    avg = sum(all_solutions) / len(all_solutions)
    std = math.sqrt(sum((avg - g) ** 2 for g in all_solutions) / len(all_solutions))
    print(the_best)
    print(the_worst)
    print(avg)
    print(std)
    # b, ci, w, all_solutions = greedy_algorithm_for_every_city(cities)
    # avg = sum(all_solutions)/len(all_solutions)
    # std = math.sqrt(sum((avg - g) ** 2 for g in all_solutions)/len(all_solutions))
    # print(count_distance(cities, b))
    # print(count_distance(cities, w))
    # print(avg)
    # print(std)
