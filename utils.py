import random
import math


def generate_genotype(length):
    left_cities = {}
    genotype = []
    for i in range(1, length + 1):
        left_cities[i] = i
    for i in range(1, length + 1):
        number_of_city = random.randrange(1, length + 1)
        while number_of_city not in left_cities:
            number_of_city = random.randrange(1, length + 1)
        del left_cities[number_of_city]
        genotype.append(number_of_city)
    return genotype


def count_distance(cities, genotype):
    distance = 0
    for i, g in enumerate(genotype):
        if i == len(genotype) - 1:
            d = count_distance_between_cities(cities[g], cities[genotype[0]])
            distance += d
        else:
            d = count_distance_between_cities(cities[g], cities[genotype[i + 1]])
            distance += d
    return distance


def count_distance_between_cities(city_a, city_b):
    return math.sqrt((city_a.x - city_b.x) ** 2 + (city_a.y - city_b.y) ** 2)
