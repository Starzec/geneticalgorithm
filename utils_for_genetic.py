import random

from models import Individual
from utils import generate_genotype, count_distance


def mutate_swap(genotype, p):  # p podawane w postaci 0.xx
    for i, g in enumerate(genotype):
        rand = random.random()
        if rand <= p:
            rand_gen = random.randrange(1, len(genotype) + 1)
            index_of_rand_gen = genotype.index(rand_gen)
            old_gen = genotype[i]
            genotype[i] = rand_gen
            genotype[index_of_rand_gen] = old_gen


def ordered_crossover(genotype_a, genotype_b):
    length = len(genotype_a)
    first_cut_place = random.randrange(0, length + 1)
    second_cut_place = random.randrange(0, length + 1)
    while first_cut_place == second_cut_place or \
            (first_cut_place == 0 and second_cut_place == length) or \
            (first_cut_place == length and second_cut_place == 0):
        first_cut_place = random.randrange(0, length + 1)
        second_cut_place = random.randrange(0, length + 1)
    if first_cut_place > second_cut_place:
        temp = first_cut_place
        first_cut_place = second_cut_place
        second_cut_place = temp
    cut_genotype = genotype_a[first_cut_place:second_cut_place]
    list_to_append_to_back = []
    for g in genotype_b:
        if len(list_to_append_to_back) < first_cut_place and g not in cut_genotype:
            list_to_append_to_back.append(g)
        elif g not in cut_genotype:
            cut_genotype.append(g)
    list_to_append_to_back.extend(cut_genotype)
    return list_to_append_to_back


def select_tournament(population, tour, pop_size):
    if tour > pop_size:
        tour = pop_size
    cut_place = random.randrange(0, pop_size)
    if cut_place + tour > pop_size:
        if cut_place - tour < 0:
            cut_place = 0
        elif cut_place - tour == 0:
            cut_place -= tour
        else:
            cut_place -= tour + 1
    the_best, _ = find_the_best_and_the_worst_in_population(population[cut_place:cut_place + tour])
    return the_best.genotype.copy()


def select_roulette(population):
    sum_of_distance = sum(1/p.distance for p in population)
    actual_start_point = .0
    point = random.random()
    for p in population:
        if actual_start_point <= point < actual_start_point + ((1/p.distance) / sum_of_distance):
            return p.genotype.copy()
        actual_start_point += (1/p.distance) / sum_of_distance


def find_the_best_and_the_worst_in_population(population):
    the_best = population[0]
    the_worst = population[0]
    for p in population:
        if p.distance < the_best.distance:
            the_best = p
        if p.distance > the_worst.distance:
            the_worst = p
    return the_best, the_worst


def append_random_individuals(population, pop_size, percent, cities):  # percent in notation 0.xx
    number_of_pop = int(percent * pop_size)
    for _ in range(number_of_pop):
        genotype = generate_genotype(len(cities))
        population.append(Individual(genotype, count_distance(cities, genotype)))